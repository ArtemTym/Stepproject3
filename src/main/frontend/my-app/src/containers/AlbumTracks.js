import React, {Component} from 'react'
import '../App.css';
import {connect} from "react-redux";
import {Link} from "react-router-dom";
import play from "../play.png"
import routes from "../constants/routes";




class AlbumTracks extends Component {

    render() {


        return (<div><h3 className="App-get-tracks">Tracks</h3>
            {this.props.album && this.props.album.tracks.map(track =>
                <li className="App-track-name"  key={track.id}>
                    {track.trackName}
                    <Link className="App-play-track" to={routes.playMusic.href + track.id}>
                    <img src={play} alt="play"/>
                    </Link>
                    <div className="heart-light"></div>
                </li>)}

        </div>)
    }
}
const mapStateToProps = (state, ownProps) => ({

    album: state.music.albums.find(album => album.id === +ownProps.match.params.albumId)
});
export default connect(mapStateToProps)(AlbumTracks)