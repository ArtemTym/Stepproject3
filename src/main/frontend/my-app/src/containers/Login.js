import React, {Component} from 'react'
import settings from "../constants/settings";

import LoginForm from "../components/forms/LoginForm";
import {isAuth, putCurrentUser} from "../actions/actions";
import {connect} from "react-redux";
import {Redirect} from "react-router-dom";
import {getUser} from "../utils/Utils";


class Login extends Component {
    constructor(props) {
        super(props)

        this.state = {
            isAuthenticated: false,
            isError: false,
            errorMessage: null
        }
    }

    handleSubmit = (values) => {
        const url = settings.api + "/auth";
        const {userName, password} = values;
        localStorage.setItem('authUserName', userName);
        localStorage.setItem('authUserPassword', password);

        const params = {
            method: "POST",
            headers: {'Content-Type': 'application/json; charset=utf-8'},
            body: JSON.stringify({
                userName: userName,
                userPassword: password})
        };
        fetch(url, params)

            .then(res => {
                if (res.status  === 200){
                   return res.json()
                       .then(body => {
                           localStorage.setItem('token', body.token)
                           this.setState({isAuthenticated: true});
                           this.props.isAuth(this.state.isAuthenticated);
                           getUser(data => {
                               this.props.putCurrentUser(data);});
                       })

                } else {
                    this.setState({isError: true, errorMessage: "Bad Username or password. Please, try again"})
                }
            });

    };


    render() {
        return(
<React.Fragment>
            <LoginForm onSubmit={this.handleSubmit}/>
    {this.state.isError && <div className="error-auth">{this.state.errorMessage}</div>}
    {this.state.isAuthenticated && <Redirect to="/user"/>}
    </React.Fragment>
        );
    }
}

const mapStateToProps = (state) => {
    return {currentUser: state.authentication.user}
};

const mapDispatchToProps = (dispatch) =>{
    return {
        isAuth: (data) => {
            dispatch(isAuth(data));
        },
        putCurrentUser: (data) => {
            dispatch(putCurrentUser(data));
        }
    }
};


export default connect (mapStateToProps,mapDispatchToProps)(Login)




