import React, {Component} from 'react'
import {connect} from "react-redux";
import {deleteCurrentUser, isAuth} from "../actions/actions";

class Logout extends Component{

    doLogout = () => {
        localStorage.removeItem('token');
        localStorage.removeItem('authUserName');

        this.props.isAuth(false);
        this.props.deleteUser();
    };

    render() {
return (

    <button onClick={this.doLogout}>Logout</button>
)
    }

}

const mapStateToProps = (state) => {
    return {auth: state.authentication.isAuthenticated}
};

const mapDispatchToProps = (dispatch) =>{
    return {
        isAuth: (data) => {
            dispatch(isAuth(data));
        },
        deleteUser: () => {
            dispatch(deleteCurrentUser());
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Logout)