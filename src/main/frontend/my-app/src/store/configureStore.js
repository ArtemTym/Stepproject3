import { createStore, applyMiddleware } from 'redux'
import albumsReducer from '../reducers/music'
import combineReducers from "redux/es/combineReducers";
import {composeWithDevTools} from "redux-devtools-extension";
import thunk from "redux-thunk";
import {reducer as formReducer} from 'redux-form'
import authenticationReducer from "../reducers/authentication";


const reducers = {
    music: albumsReducer,
    authentication: authenticationReducer,
    form: formReducer


};

const rootReducer = combineReducers(reducers);

export default function configureStore() {
    const store = createStore(rootReducer, composeWithDevTools(applyMiddleware(thunk)))
    return store
}