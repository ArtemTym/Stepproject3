import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import {BrowserRouter} from 'react-router-dom'
import {Provider} from 'react-redux'
import configoreStore from "./store/configureStore"
import App from './App';


const store = configoreStore()

ReactDOM.render(<Provider store={store}><BrowserRouter><App/></BrowserRouter>
</Provider>, document.getElementById('root') );
