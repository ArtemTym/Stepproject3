import * as types from '../constants/actionTypes';


export function addAlbums(albums) {
    return{
        type: types.ADD_ALBUM,
        albums
    }
};

export function isAuth(authBool) {
return{
    type: types.IS_AUTH,
    authBool
}
};

export function deleteAlbums() {
    return{
        type: types.DELETE_ALL_ALBUMS,
        deleteAlbums:[]
    }
}

export function putCurrentUser(user) {
    return{
        type: types.SHOW_CURRENT_USER,
        user
    }
}

export function deleteCurrentUser() {
    return{
        type: types.DELETE_CURRENT_USER,
        deleteUser: []
    }

}

export function addTracks(tracks) {
    return{
        type: types.ADD_TRACKS,
        tracks
    }
};




