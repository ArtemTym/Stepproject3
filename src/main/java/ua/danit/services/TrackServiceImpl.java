package ua.danit.services;

import com.sun.org.apache.bcel.internal.generic.ALOAD;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.danit.model.Album;
import ua.danit.model.Track;
import ua.danit.repository.AlbumsRepository;
import ua.danit.repository.TrackRepository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


@Service
public class TrackServiceImpl implements TrackService {

    @Autowired
    private TrackRepository trackRepository;

    @Autowired
    private AlbumsRepository albumsRepository;

    @Override
    public List<Track> getAll() {
        return (List<Track>) trackRepository.findAll();
    }

    @Override
    public Track getById(Long id) {
        return trackRepository.findOne(id);
    }

    @Override
    public void deleteTrack(Long id) {
        trackRepository.delete(id);
    }


    @Override
    public Track addTrack(Track track) {

        String album = track.getAlbum().getAlbumName();
        if (album == null) {
            return trackRepository.save(track);
        } else {
            Album oldAlbum = albumsRepository.findByAlbumName(album);
            if (oldAlbum != null) {
                track.getAlbum().setId(oldAlbum.getId());
                return trackRepository.save(track);
            } else {
                Album newAlbum = track.getAlbum();
                track.setAlbum(null);
                Track newTrack = trackRepository.save(track);
                List<Track> addTracks = new ArrayList<>();
                newTrack.setAlbum(newAlbum);
                addTracks.add(newTrack);
                newAlbum.setTracks(addTracks);
                albumsRepository.save(newAlbum);
                return newTrack;
            }
        }

    }

    @Override
    public Track updateTrack(Track track, Long id) {

        Track oldTrack = trackRepository.findOne(id);
        oldTrack.setTrackName(track.getTrackName());
        return trackRepository.findOne(id);
    }

    @Override
    public Track uploadTrack(Track track) {
        return track;
    }


}


//if (album != null){
//        Album createdAlbum = albumsRepository.findByAlbumName(album);
//        if (createdAlbum != null){
//        track.getAlbum().setId(createdAlbum.getId());
//        return trackRepository.save(track);
//        } else {
//        Album newAlbum = new Album();
//        newAlbum.setAlbumName(album);
//        newAlbum.setCoverPath(track.getAlbum().getCoverPath());
//        newAlbum.setSingerName(track.getAlbum().getSingerName());
//        List<Track> addTracks = new ArrayList<>();
//        addTracks.add(track);
//        newAlbum.setTracks(addTracks);
//        albumsRepository.save(newAlbum);
//        }
//        } else {
//        trackRepository.save(track);
//        }
//        return trackRepository.save(track);
