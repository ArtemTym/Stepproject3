package ua.danit.services;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;

import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;

@Service
public class AmazonClient  {




    private AmazonS3 storage = AmazonS3ClientBuilder.standard()
        .withCredentials(new AWSStaticCredentialsProvider(
        new BasicAWSCredentials(
        "AKIAJSXP7KMFCLMK3TDA",
        "PGoOwo1I34Wq4FqvJ6ugF/yXzFIx4VQchj5OONfD"
        )))
        .withRegion("eu-central-1").build();

    private File convertMultiPartToFile(MultipartFile file) throws IOException {
        File convFile = new File(file.getOriginalFilename());
        FileOutputStream fos = new FileOutputStream(convFile);
        fos.write(file.getBytes());
        fos.close();
        return convFile;
    }

    private String generateFileName(MultipartFile multiPart) {
        String name = multiPart.getOriginalFilename();
        return name;
    }


    public String uploadNewTrack(MultipartFile file) {
        String fileUrl = "";
        try {
            File file1 = convertMultiPartToFile(file);
            String fileName = generateFileName(file);
            fileUrl = "https://s3.eu-central-1.amazonaws.com/music-box-tt-storage/tracks/" + fileName;
            storage.putObject(new PutObjectRequest("music-box-tt-storage/tracks", fileName, file1)
                    .withCannedAcl(CannedAccessControlList.PublicRead));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return fileUrl;
    }

    public String uploadCover(MultipartFile file) {

        String fileUrl = "";
        try {
            File file1 = convertMultiPartToFile(file);
            String fileName = generateFileName(file);
            fileUrl = "https://s3.eu-central-1.amazonaws.com/music-box-tt-storage/covers/" + fileName;
            storage.putObject(new PutObjectRequest("music-box-tt-storage/covers", fileName, file1)
                    .withCannedAcl(CannedAccessControlList.PublicRead));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return fileUrl;
    }
}
