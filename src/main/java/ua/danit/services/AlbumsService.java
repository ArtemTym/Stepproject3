package ua.danit.services;


import ua.danit.model.Album;

public interface AlbumsService {


    Iterable<Album> getAll();


    Album getByID(Long id);

    void deleteAlbum(Long id);

    Album updateAlbum(Album album, Long id);

    Album addAlbum(Album albums);

    void deleteCover(Long id);
}


