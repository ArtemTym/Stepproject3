package ua.danit.services;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;
import ua.danit.model.Album;
import ua.danit.repository.AlbumsRepository;
import ua.danit.repository.TrackRepository;

import java.util.List;

@Service
public class AlbumsServiceImpl implements AlbumsService {

    @Autowired
    private AlbumsRepository albumsRepository;

    @Autowired
    private TrackRepository trackRepository;


    @Override
    public List<Album> getAll() {
        return (List<Album>) albumsRepository.findAll();
    }

    @Override
    public Album getByID(Long id) {
        return albumsRepository.findOne(id);
    }

    @Transactional
    @Override
    public void deleteAlbum(Long id) {

        Album album = getByID(id);
        trackRepository.deleteByAlbum(album);
        albumsRepository.delete(id);
    }

    @Override
    public Album updateAlbum(Album album, Long id) {
    Album oldAlbum = albumsRepository.findOne(id);
    oldAlbum.setAlbumName(album.getAlbumName());
        return albumsRepository.save(oldAlbum);
    }

    @Override
    public Album addAlbum(Album album) {
        return albumsRepository.save(album);
    }

    @Override
    public void deleteCover(Long id) {
        Album album = albumsRepository.findOne(id);
        album.setCoverPath(null);
        albumsRepository.save(album);

    }


}



