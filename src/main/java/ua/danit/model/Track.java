package ua.danit.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "tracks_tt")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Track {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;


    @Column(name = "trackName")
    private String trackName;

    @Column(name = "trackPath")
    private String trackPath;


    @ManyToOne
    @JoinColumn(name = "album_id")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Album album;




}
