package ua.danit.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;


@Entity
@Table(name = "likes_tt")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Like {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;




    @ManyToOne
    @JoinColumn(name = "track_id")
    private Track track;


}



