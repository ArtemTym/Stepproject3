package ua.danit.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ua.danit.model.Album;
import ua.danit.model.Track;
import java.util.List;

@Repository
public interface TrackRepository extends JpaRepository<Track, Long> {



    void deleteByAlbum(Album album);
}
