package ua.danit.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ua.danit.model.Track;
import ua.danit.services.AmazonClient;
import ua.danit.services.TrackService;


import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("api/tracks")
@CrossOrigin("http://localhost:3000")
public class TrackController {

    @Autowired
    private TrackService trackService;

    @Autowired
    private AmazonClient amazonClient;




    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity<List<Track>> getAll(){ return ResponseEntity.ok().body(trackService.getAll()); }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Track getById(@PathVariable("id") Long id) {
        return trackService.getById(id);
    }

    @PutMapping("/{id}")
//    @PreAuthorize("hasRole('ADMIN')")
    public Track updateTrack (@RequestBody Track track, @PathVariable("id") Long id ){
        return trackService.updateTrack(track, id); }

    @DeleteMapping(value = "/{id}")
//    @PreAuthorize("hasRole('ADMIN')")
    public void deleteTrack (@PathVariable("id") Long id){ trackService.deleteTrack(id); }

    @PostMapping(value = "/add")
//    @PreAuthorize("hasRole('ADMIN')")
    public Track addTrack(@RequestBody Track track){
       return trackService.addTrack(track);
    }


     @PostMapping("/upload")
    public String uploadTrack ( @RequestPart(value = "file") MultipartFile file)
     throws IOException {
       return amazonClient.uploadNewTrack(file);

    }

}

