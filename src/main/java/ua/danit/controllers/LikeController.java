//package ua.danit.controllers;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.*;
//
//
//import ua.danit.model.Like;
//import ua.danit.services.LikeService;
//
//import java.util.List;
//
//@RestController
//@RequestMapping("api/like")
//public class LikeController {
//
//    @Autowired
//    private LikeService likeService;
//
//    @RequestMapping(value = "", method = RequestMethod.GET)
//    public ResponseEntity<List<Like>> getAll(){ return ResponseEntity.ok().body(likeService.getAll()); }
//
//    @RequestMapping(value = "/u{id}", method = RequestMethod.GET)
//    public List<Like> getLikedById(@PathVariable("id") Long id) {
//        return likeService.getLikedById(id);
//    }
//
//    @PutMapping("?liked=true")
//    public Like updateLikedTrack (@RequestBody Like like, Long userId, Long track_id ){
//        return likeService.updateLikedTrack(like, userId, track_id); }
//
//
//    @PostMapping(value = "tracks/{id}/?liked=true")
//    public Like addLikeToTrack(@RequestBody Like like, @PathVariable("id") Long id){
//        return likeService.addLikeToTrack(like);}
//
//    @RequestMapping(value = "/t{id}", method = RequestMethod.GET)
//    public String test(@PathVariable("id") Long id) {
//        String query = "SELECT track_id FROM likes_tt WHERE u_id = " +id+";";
//        return query;
//    }
//}
//

