package ua.danit.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ua.danit.model.Album;
import ua.danit.services.AlbumsService;
import ua.danit.services.AmazonClient;

import java.io.IOException;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("api/albums")
public class AlbumsController {

    @Autowired
    private AlbumsService albumsService;

    @Autowired
    private AmazonClient amazonClient;



    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity<Iterable<Album>> getAll() {

        return ResponseEntity.ok().body(albumsService.getAll());
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)

    public Album getById(@PathVariable("id") Long id) {
        return albumsService.getByID(id);
    }

    @PutMapping("{id}")
//    @PreAuthorize("hasRole('ADMIN')")
    public Album updateAlbum(@RequestBody Album album, @PathVariable Long id) {

        return albumsService.updateAlbum(album,id);

    }

    @DeleteMapping(value = "/{id}")
//    @PreAuthorize("hasPermission(1, 'ADMIN')")
    public void  deleteAlbum(@PathVariable("id") Long id){
        albumsService.deleteAlbum(id);
    }

    @PostMapping(value = "/add")
    public Album addAlbum(@RequestBody Album album){
        return albumsService.addAlbum(album);
    }


    @DeleteMapping("/{id}/cover")
    public void deleteCover(@PathVariable("id") Long id){
        albumsService.deleteCover(id);
    }

    @PostMapping("/add/cover")
    public String uploadCover(@RequestPart(value = "file")MultipartFile file) throws IOException{
        return amazonClient.uploadCover(file);
    }


}
